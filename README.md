# Running docker in docker (dind) in Gitlab CI with pre-loaded docker images

Docker in docker (dind) is really useful solution which allows to isolate different CI builds completely from each other, but still allowing to use for example docker-compose commands inside test scripts.

Basically it means that your CI host machine has docker installed and each CI build is started as a separate container in host machine's docker service.

However if you want to use docker commands inside your CI script to setup own services etc. it means that you would need to either give your CI runner container access to the host machine's docker service or docker container need provide docker service of its own which is called docker in docker (dind).

If we provide access to host machines docker service, each build is not anymore completely isolated, but all parallel builds will be able to see and access all container running in the host machine, which is not desirable.

To have complete isolation and being able to have clean docker environment in each build dind can be used, but it has also some drawbacks; one cannot use images cached in host machine to start container so every build must get images somehow to be installed to their local cache.

Most of the sites prefer providing local docker registry, where from images are downloaded or to use docker save and docker load commands to load images from file. Both solutions are still quite slow when installing multiple giga bytes of images and it can easily take minutes to prepare dind to have correct images before starting services locally.

In this approach I will create dind container image first and then load all the required images there and only after that save it as new dind image with all the required images preloaded.

There were quite a few obstacles that was needed to go through before everything was working as intented.

Firstly docker does not support running images from docker's layer file (TODO: check correct name) system, so we had to create virtual loopback file system with ext4 format and setup docker to use that for its cache directory.

Second problem was that file system must be resized to be correct size before saving new docker image with it to prevent docker filesystem from expanding sparse ext4 disk image when writing it to docker's layers. Sparse files are not supported because go languages gzip implementation doesn't support sparse files currently (TODO: add link to issue about it).

Next problem was that busybox didn't support all the required flags (TODO: find the issue + fix) to be able to resize mounted drive to smaller or bigger.

When all hassle with resizing image and writing it to new docker in docker image with preloaded images was resolved there was new problem. When mounting loopback device from image with preloaded images to use in CI script it took really long time to mount the data. With some research and help from more knowledgeable friend we figured out that when mounting file from docker's file system and trying to write data there system had to do copy-on-write operation for the file that contained images of dind container. When host system was running ext4 file system host had to make second copy of the loopback filesystem thus causing delay on container startup and doubling container size, becuase file had to be copied completely from docker file system to ext4.

We were able to resolve that issue by installing host machine with gitlab-runner to btrfs filesystem, which only stores changes to files as diffs instead of doing full copy-on-write.

After all above in place we were able to start dind container with preloaded images instantly and start new container immeditely without need to pull images from any external registries.

So our CI initialization time did drop from almost an hour to just few seconds.

# Getting started

## Install Ubuntu 18 which will be running your builds (virtual machine works just fine)

This machine will serve as your gitlab-runner. It must be installed with btrfs file system to prevent copy-on-write when running dind containers with preloaded images.

During installation, selecting minimal installation and setting manually one btrfs partition mounted to / should be enough.

### Install and setup gitlab-runner

Official docs: https://docs.gitlab.com/runner/

Check url and token to insert from your gitlab's `Settings / CI/CD / Runners page`

e.g. url: https://gitlab.com/ and token: XXXXXXXXXXXXXXXXXX

```
sudo apt install curl
sudo apt install docker.io
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker gitlab-runner

# reboot to get new group to take effect and then following command should be able to
# connect to host machine's docker socket /var/run/docker.sock

docker images

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner

sudo gitlab-runner register
```

For executor select `docker` and get gitlab url and token from your gitlab page.

Now your `/etc/gitlab-runner/config.toml` should look something like this (there are some modifications done):

```
root@gitlab-runner:~# cat /etc/gitlab-runner/config.toml

#
# Gitlab-runner config to run this CI script
#

# /etc/gitlab-runner/config.toml
#
# generate by running:
# sudo gitlab-runner register
#
# Just make sure that afterwards there is following settings added:
#
# pull_policy = "if-not-present"
# volumes = ["/var/run/docker.sock:/var/run/docker-host.sock", "cache"]
# privileged = true
#

# if you have lots of memory and IO and CPU available this can be set to
# higher number to run multiple builds concurrently
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "dind-runner"
  url = "https://gitlab.com/"
  token = "xxxxxxxxxxxxxxxxxxx"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    # run in privileged mode to allow running dind containters
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    shm_size = 0
    # share host machines docker service socket as /var/run/docker-host.sock
    # to prevent exposed socket from colliding with default socket created in dind
    # container
    volumes = ["/var/run/docker.sock:/var/run/docker-host.sock", "/cache"]
    # prevent pulling image if it already exists in cache
    pull_policy = "if-not-present"
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.custom]
    run_exec = ""
```

To allow docker executor to run docker in docker containers, containers must be ran with privilegd mode and also it is useful to share host machine's docker unix socket to containers so that they can use it when convenient (for example when building dind image with preloaded images). So some changes to configuration are needed.

That is somewhat dangerous so it is recommended to have dedicated virtual machine or machine for runner or not to allow using the runner for external pull requests.

After fixing `/etc/gitlab-runner/config.toml` run `service gitlab-runner restart` to apply changes.

Now setting up gitlab-runner should be all done and new runner should be visible in your gitlab page.

## Writing first giltab CI task to test the runner

Now gitlab runner should be visible in gitlab's `Settings / CI/CD / Runners` page.

To create first base image that is used to run your application create `.gitlab-ci.yml` file to repository root.

```
stages:
  - build-base-images

# Build image containing all the required commands like node etc.
# to be able to run your application code. This is stored to host machine's
# docker service so gitlab-runner can access it when it creates runner container.
# Because image is duild with host machines docker service, layer cache will prevent
# need to recreate it on every CI run.
build-testrun-env:
  stage: build-base-images
  image: docker:18
  variables:
    DOCKER_HOST: unix:///var/run/docker.sock
  script:
    - docker build -t local/testrunenv ./.gitlab-runenv
```

and copy `.gitlab-runenv/`directory to your project and modify `Dockerfile` to contain all software necessary to run your application.

Then commit new files and push them to gitlab and see that CI runs correctly.

It should look something like this:

```
Running with gitlab-runner 12.1.0 (de7731dd)
  on dind-runner TzKz9-Xr
Using Docker executor with image docker:18 ...
Running on runner-TzKz9-Xr-project-13858216-concurrent-0 via gitlab-runner...
Fetching changes with git depth set to 50...

...

$ docker build -t local/testrunenv ./.gitlab-runenv

...

Job succeeded
```

## Setting up test runner task with dind service for your app

In Gitlab CI one can setup services, which are then seen in certain hostname. Usually dind service is setup like that and now we create test setup with docker in docker enabled.

So test runner task will actually have separate docker service from gitlab-runner host machine's docker service.

```
test:
  stage: test
  image: local/testrunenv
  services:
    - docker:18-dind
  script:
    - node --version
    - docker images
    - export DOCKER_HOST=$HOST_DOCKER_SOCKET
    - echo "Show host machine's images"
    - docker images
```

This task should now have its isolated docker service which doesn't have any preloaded images yet.

Official documentation how to set this up is found from https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor

## Building custom dind service image with pre-pulled docker images

Now our basic test setup is complete with isolated dind service available for each test runner. Next and most difficult task will be to create custom dind image which contains pre-pulled images for test runner to use.

I will explain this step by step so that after each step one can make sure that everything works correctly which also helps debugging the system later on.

### Build custom dind image which is stored locally on host machine's docker service

Main reason why we build everything to gitlab-runner machine's docker service is to allow gitlab-runner to access the image for setting up the service. Images could be pushed to some shared registry as well, but it is out of scope of this tutorial.

First we create new directory `.gitlab-caching-dind/` for custom dind Dockerfile. We use separate directories for each to minimize build context that needs to be uploaded to docker image building phase.

Dockerfile is really simple and it does only force dind to use overlay2 storage driver for storing docker image layers.

```
FROM docker:18-dind

# force overlay2 driver
CMD ["--storage-driver=overlay2"]
```

and then we setup new build stage for creating / refreshing this custom dind image and setup our test task to use it instead of default docker:dind image.

```
stages:
  - build-base-images
  - refresh-dind
  - test
```

```
build-dind-with-cache:
  stage: refresh-dind
  image: docker:18
  variables:
    DOCKER_HOST: $HOST_DOCKER_SOCKET
  script:
    - cd $CI_PROJECT_DIR/.gitlab-caching-dind
    - docker build -t local/cached-dind ./.gitlab-runenv
```

```
test:
  stage: test
  image: local/testrunenv
  services:
    - name: local/cached-dind
      alias: docker
  script:
    - docker images
```

### Setup custom dind image to have loopback ext4 partition for pre-pulled images

Since docker's layer filesystem cannot be used to host docker images and temporary volume created in dind container is not preserved we need to setup loopback device file which is mounted as ext4 partition and then written to docker layer and on container startup it is mounted again.

For that we need to modify dind containers entrypoint script to create that partition and mount it.

So first we add custom `.gitlab-caching-dind/custom-entrypoint.sh` and we just call original entrypoint from it to be able to add more tasks there later on.

```
FROM docker:18-dind

COPY custom-entrypoint.sh /usr/local/bin/custom-entrypoint.sh
RUN chmod gou+x /usr/local/bin/custom-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/custom-entrypoint.sh"]

# force overlay2 driver
CMD ["--storage-driver=overlay2"]
```

Next we should setup docker to use custom data-root-dir so that pre-pulled images will be persisted in docker's layer files. Because layer filesystem cannot be used to serve docker images, we just create disk image file with ext4 filesystem and mount it to correct place and store that disk to layer file system.

For that we need to modify `.gitlab-caching-dind/Dockerfile` like this:

```
FROM docker:18-dind

COPY custom-entrypoint.sh /usr/local/bin/custom-entrypoint.sh
RUN chmod gou+x /usr/local/bin/custom-entrypoint.sh

RUN mkdir -p /var-lib-docker

ENTRYPOINT ["/usr/local/bin/custom-entrypoint.sh"]

# force overlay2 driver and use custom data-root-dir
CMD ["--storage-driver=overlay2", "--data-root=/var-lib-docker"]
```

We also need to add creation of disk image and mounting it to our `.gitlab-caching-dind/custom-entrypoint.sh`

```
# to start we create just 1GB sparse filesystem and add
# more space if it is about to fill up
if [ ! -e /var-lib-docker.loopback.ext4 ]; then
  dd of=/var-lib-docker.loopback.ext4 bs=1 seek=1G count=0
  /sbin/mkfs.ext4 -q /var-lib-docker.loopback.ext4
fi

# and mount file system to data-root-dir
mount -t ext4 -o loop /var-lib-docker.loopback.ext4 /var-lib-docker
```
